require.config({
  paths: {
    jquery: "../vendor/jquery.min",
    "bootstrap.bundle": "../vendor/bootstrap.bundle.min",
    admin: "../view/admin",
    d3: "../vendor/d3.4.3.0.min"
  }
});

require(["jquery", "bootstrap.bundle", "admin", "d3"], function(
  $,
  bb,
  admin,
  d3
) {
  admin.init();
  //CONTAINER_INFO.HTML CHARTS

  // #region id='d31'
  var data = [
    { name: "cats", count: 6, percentage: 2, color: "#c1392f" },
    { name: "dogs", count: 10, percentage: 8, color: "#ecf0f1" }
  ];
  var totalCount = 40; //calcuting total manually

  var width = 300,
    height = 300,
    radius = 130;

  var arc = d3
    .arc()
    .outerRadius(radius - 10)
    .innerRadius(100);

  var pie = d3
    .pie()
    .sort(null)
    .value(function(d) {
      return d.count;
    });

  var svg = d3
    .select("#d31")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  var g = svg
    .selectAll(".arc")
    .data(pie(data))
    .enter()
    .append("g");

  g.append("path")
    .attr("d", arc)
    .style("fill", function(d, i) {
      return d.data.color;
    });

  g.append("text")
    .attr("transform", function(d) {
      var _d = arc.centroid(d);
      _d[0] *= 1.5; //multiply by a constant factor
      _d[1] *= 1.5; //multiply by a constant factor
      return "translate(" + _d + ")";
    })
    .attr("dy", ".50em")
    .style("text-anchor", "middle")
    .text();

  g.append("text")
    .attr("text-anchor", "middle")
    .attr("font-size", "4em")
    .attr("y", 20)
    .text(totalCount);
  // #endregion

  //   #region id='d32'
  var data = [
    { name: "cats", count: 14, percentage: 2, color: "#4fcc72" },
    { name: "dogs", count: 3, percentage: 8, color: "#ecf0f1" }
  ];
  var totalCount = 85; //calcuting total manually

  var width = 300,
    height = 300,
    radius = 130;

  var arc = d3
    .arc()
    .outerRadius(radius - 10)
    .innerRadius(100);

  var pie = d3
    .pie()
    .sort(null)
    .value(function(d) {
      return d.count;
    });

  var svg = d3
    .select("#d32")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  var g = svg
    .selectAll(".arc")
    .data(pie(data))
    .enter()
    .append("g");

  g.append("path")
    .attr("d", arc)
    .style("fill", function(d, i) {
      return d.data.color;
    });

  g.append("text")
    .attr("transform", function(d) {
      var _d = arc.centroid(d);
      _d[0] *= 1.5; //multiply by a constant factor
      _d[1] *= 1.5; //multiply by a constant factor
      return "translate(" + _d + ")";
    })
    .attr("dy", ".50em")
    .style("text-anchor", "middle")
    .text();

  g.append("text")
    .attr("text-anchor", "middle")
    .attr("font-size", "4em")
    .attr("y", 20)
    .text(totalCount);
  //#endregion
});