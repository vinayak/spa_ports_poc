require.config({
  paths: {
    jquery: "../vendor/jquery.min",
    "bootstrap.bundle": "../vendor/bootstrap.bundle.min",
    admin: "../view/admin",
    d3: "../vendor/d3.4.3.0.min"
  }
});

require(["jquery", "bootstrap.bundle", "admin", "d3"], function(
  $,
  bb,
  admin,
  d3
) {
  admin.init();

  //#region id='d33'
  // set the dimensions and margins of the graph
  var margin = { top: 20, right: 20, bottom: 30, left: 40 },
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

  // set the ranges
  var x = d3
    .scaleBand()
    .range([0, width])
    .padding(0.1);
  var y = d3.scaleLinear().range([height, 0]);

  // append the svg object to the body of the page
  // append a 'group' element to 'svg'
  // moves the 'group' element to the top left margin
  var svg = d3
    .select("#d33")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var data = [
    {
      salesperson: "FSSAI",
      sales: 33
    },
    {
      salesperson: "Textile department",
      sales: 12
    },
    {
      salesperson: "Explosion lab",
      sales: 41
    },
    {
      salesperson: "Ammunition department",
      sales: 16
    },
    {
      salesperson: "PQ",
      sales: 59
    },
    {
      salesperson: "AQ",
      sales: 38
    },
    {
      salesperson: "Drugs",
      sales: 21
    },
    {
      salesperson: "WCCB",
      sales: 59
    }
  ];
  // get the data
  //   d3.json(data, function(error, data) {
  // if (error) throw error;

  // format the data
  data.forEach(function(d) {
    d.sales = +d.sales;
  });

  // Scale the range of the data in the domains
  x.domain(
    data.map(function(d) {
      return d.salesperson;
    })
  );
  y.domain([
    0,
    d3.max(data, function(d) {
      return d.sales;
    })
  ]);

  // append the rectangles for the bar chart
  svg
    .selectAll(".bar")
    .data(data)
    .enter()
    .append("rect")
    .attr("class", "bar")
    .attr("x", function(d) {
      return x(d.salesperson);
    })
    .attr("width", x.bandwidth())
    .attr("y", function(d) {
      return y(d.sales);
    })
    .attr("height", function(d) {
      return height - y(d.sales);
    });

  // add the x Axis
  svg
    .append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));

  // add the y Axis
  svg.append("g").call(d3.axisLeft(y));
  //#endregion

  //#region id='d34'

  var sales = [
    { company: "CMA CGM", count: 33 },
    { company: "Hapag-Lloyd", count: 12 },
    { company: "Hanjin Shipping", count: 41 },
    { company: "MSC", count: 16 },
    { company: "UASC", count: 59 },
    { company: "ZIM", count: 38 },
    { company: "MAERSK", count: 59 },
    { company: "NYK", count: 21 }
  ];

  var pie = d3.pie().value(function(d) {
    return d.count;
  });

  var slices = pie(sales);

  var arc = d3
    .arc()
    .innerRadius(0)
    .outerRadius(120);

  // helper that returns a color based on an ID
  var color = d3.scaleOrdinal(d3.schemeCategory10);

  var svg = d3
    .select("#d34")
    .append("svg")
    .attr("class", "pie");
  var g = svg.append("g").attr("transform", "translate(250, 140)");

  var arcGraph = g
    .selectAll("path.slice")
    .data(slices)
    .enter();
  arcGraph
    .append("path")
    .attr("class", "slice")
    .attr("d", arc)
    .attr("fill", function(d) {
      return color(d.data.company);
    });

  arcGraph
    .append("text")
    .attr("transform", function(d) {
      return "translate(" + arc.centroid(d) + ")";
    })

    .attr("dy", "0.35em")
    .text(function(d) {
      return d.data.count;
    });
  // building a legend is as simple as binding
  // more elements to the same data. in this case,
  // <text> tags
  svg
    .append("g")
    .attr("class", "legend")
    .selectAll("text")
    .data(slices)
    .enter()
    .append("text")
    .text(function(d) {
      return "• " + d.data.company;
    })
    .attr("fill", function(d) {
      return color(d.data.company);
    })
    .attr("y", function(d, i) {
      return 20 * (i + 1);
    });

  //#endregion
});