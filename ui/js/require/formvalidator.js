require.config({
    paths: {
        'jquery': '../vendor/jquery.min',
        'bootstrap.bundle' : '../vendor/bootstrap.bundle.min',
        'admin':'../view/admin',
        'formvalidatorapi':'../api/formvalidator_api',

    }
});

require(['jquery',
	'bootstrap.bundle',
	'admin'], function($,bb,admin){
            admin.init();
            formvalidatorapi.init();
        
});