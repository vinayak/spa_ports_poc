require.config({
    paths: {
        'jquery': '../vendor/jquery.min',
        'bootstrap.bundle' : '../vendor/bootstrap.bundle.min',
        'admin':'../view/admin'

    }
});

require(['jquery',
	'bootstrap.bundle',
	'admin'], function($,bb,admin){
            admin.init();
        
});