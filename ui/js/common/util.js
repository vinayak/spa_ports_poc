define(['jquery'],function($){
	//fetch from the url the value of the parameter mentioned as sParam, if on existence
	getUrlParameter = function (sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	},
	//perform a post call using ajax
	ajaxPostCall = function(url, request, callback) {
	    $.ajax({
	        url: url,
	        type: 'POST',
	        data: JSON.stringify(request),
	        dataType: "json",
	        timeout: 200000,
	        headers: {
	            Accept: "application/json"
	        },
	        async: true,
	        contentType: "application/json",
	        success: function(data) {
	            callback(data);
	        },
	        error: function(x, error, m) {
	            console.log(x + " " + error + " " + m);
	        }
	    });
	  }
	return {
	"ajaxPostCall":ajaxPostCall,
	"getUrlParameter":getUrlParameter
	}
});