define(['jquery'],function($){

  var alignNavbar = function(){
    $('.navbar-sidenav [data-toggle="tooltip"]').tooltip({
      template: '<div class="tooltip navbar-sidenav-tooltip" role="tooltip" style="pointer-events: none;"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
    });
    // Toggle the side navigation
    $("#sidenavToggler").click(function(e) {
      e.preventDefault();
      $("body").toggleClass("sidenav-toggled");
      $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
      $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
    });
    // Force the toggled class to be removed when a collapsible nav link is clicked
    $(".navbar-sidenav .nav-link-collapse").click(function(e) {
      e.preventDefault();
      $("body").removeClass("sidenav-toggled");
    });
  },
  loadNavbarPartial = function(callback){
    //load side navigation bar partial
    $('#navbarpartial').load('../partial/navbar.html',callback);
  },
  loadFooterPartial=function(){
    // load footer partial
    $('#appFooterPartial').load('../partial/footer.html');
  },
  init = function (){
    $(document).ready(function(){
      loadNavbarPartial(alignNavbar);
      loadFooterPartial();
    });
  };

  return {
    "init":init
  }


});
