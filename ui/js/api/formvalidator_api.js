define(['jquery','constant','util'],function($,appConstants,util){ 

	  var validateForm = function(){
	  	var url=appConstants.host+appConstants.urlLib.formassist,
	  	//callback to handle response for constructing alerts
			handleResponse=function(data){
				var res="No results";
				if (data!=null & data.hasOwnProperty('form_score') & data.hasOwnProperty('alerts')){
					res="<table class='table'><thead><tr><th scope='col'>#</th><th scope='col'>Field</th><th scope='col'>Submitted value</th><th scope='col'>Alert</th></tr></thead><tbody>";
					for (var key in data['alerts']) {
					    if (data['alerts'].hasOwnProperty(key)) {
					    res+="<tr><th scope='row'>1</th><td>fieldname</td><td>fieldvalue</td><td>alertmessage</td></tr>"           
					        console.log(key, data['alerts'][key]);
					    }
					}
					res+="</tbody></table>"

				}
				$('#alerttable').html(res);
			},
			// submit button event handler
			submitHandler=function(){
				$('#validateform').on('click',function(){
					util.ajaxPostCall(url,
					{},//Pass here the form data as key:value 
					handleResponse);
				});
			},
	
			//initialize submit event handler and datepicker partial
			init=function(){
				$('#alerttable').hide();
				submitHandler();

			};
			return {
				"init":init
			}
	}();
	return {
		"validateForm":validateForm
	}
});