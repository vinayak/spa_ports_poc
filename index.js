var express = require('express');
var Tx = require('ethereumjs-tx');
var hash = require('object-hash');
var app = express();
var ETHEREUM_NETWORK = "http://localhost:8545"
var gaslimit=1961791
var Web3 = require('web3');
const pkey="842fd050bb1a8eeb151835766362537983b8dfedfeebbc2d07fcffd7623f9ba4"
const web3 = new Web3(new Web3.providers.HttpProvider(ETHEREUM_NETWORK));
var toAcc=null;
web3.eth.defaultAccount=null;

web3.eth.getAccounts().then(e => { 
 web3.eth.defaultAccount = e[0];
 toAcc=e[1]
}); 


function sendSigned(txData, cb) {
  const privateKey = new Buffer(pkey, 'hex');
  const transaction = new Tx(txData);
  transaction.sign(privateKey)
  const serializedTx = transaction.serialize().toString('hex')
  return web3.eth.sendSignedTransaction('0x' + serializedTx);
}




app.post('/data/', (req, res) => {
	res.setHeader("Content-Type", "application/json");
  req.on('data', function (data) {
		const objProps = JSON.parse(data);
		var hdata= hash(objProps);
		console.log(toAcc);
		web3.eth.getTransactionCount(web3.eth.defaultAccount).then(txCount => {
		  var nonce=web3.utils.toHex(txCount);
		  var val = web3.utils.toHex(web3.utils.toWei("1"));
		  const txData = {
			nonce: web3.utils.toHex(txCount),
			gasLimit: web3.utils.toHex(2500000),
			gasPrice: web3.utils.toHex(10e9), 
			to: toAcc,
			from: web3.eth.defaultAccount,
			value: val,
			data: JSON.stringify(objProps)
		  }

		cb= sendSigned(txData)
		cb.on('transactionHash', function(hash) {
            console.log("Тhash " + hash);
        })
        .on('error', function(error) {
            res.send(JSON.stringify({"error":error}));
        })
        .once('confirmation', function(confirmationNumber, receipt) {
            res.send(JSON.stringify({"res":receipt}));
            
        });
		  

		});

	});
});


app.listen(3001, function () {
  console.log('App alive');
});
