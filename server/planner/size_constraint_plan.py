__author__ = 'crusaderwolf'

from ortools.algorithms import pywrapknapsack_solver

solver = pywrapknapsack_solver.KnapsackSolver(
      pywrapknapsack_solver.KnapsackSolver.
      KNAPSACK_DYNAMIC_PROGRAMMING_SOLVER,
      'berthplanner')

def size_constraint_optimizer(itemlist,capacity):
  solver.Init(itemlist, [itemlist], [capacity])
  solver.Solve()
  listedships = [x for x in range(0, len(itemlist))
                    if solver.BestSolutionContains(x)]
  return listedships

def get_berth_plan(shiplengthlist,berthsize):
    return size_constraint_optimizer(shiplengthlist,berthsize)

def get_cargo_plan(cargolist,shipsize):
    return size_constraint_optimizer(cargolist,shipsize)

