__author__ = 'crusaderwolf'
from common_util import util, module_registry
from flask import jsonify, request
from flask_cors import CORS, cross_origin
import traceback
import logging
from planner import size_constraint_plan

"""
App initialization
"""
try:
    app = module_registry.boot_sequence()
    logging.getLogger('boot').info("booting successful")
except Exception as e:
    logging.getLogger('boot').info("Exception while booting %s", e)
    print(e)
    exit()

"""
POST api plan berth
"""
"""
{
"shiplength":[123,34,56,122,221,111],
"berthsize":300
}
"""

@app.route("/berthplanner/v1", methods=['POST'])
@cross_origin()
def berth_planner():
    try:
        requestData = util.jsonify_request(request)
        resp =size_constraint_plan.get_berth_plan(requestData.get('shiplength'),
                                 requestData.get('berthsize'))
        return jsonify(resp)
    except Exception as e:
        logging.getLogger("api").error(
            "/berthplanner/v1     fails %s", e)
        return jsonify({"err": traceback.format_exc()})


"""
POST api plan cargo load
"""
"""
{
"cargoweight":[123,34,56,122,221,111],
"shipcapacity":300
}
"""

@app.route("/cargoplanner/v1", methods=['POST'])
@cross_origin()
def cargo_planner():
    try:
        requestData = util.jsonify_request(request)
        resp =size_constraint_plan.get_cargo_plan(requestData.get('cargoweight'),
                                 requestData.get('shipcapacity'))
        return jsonify(resp)
    except Exception as e:
        logging.getLogger("api").error(
            "/cargoplanner/v1     fails %s", e)
        return jsonify({"err": traceback.format_exc()})

@app.route("/formassist/v1", methods=['POST'])
@cross_origin()
def form_assist():
    try:
        requestData = util.jsonify_request(request)

        return jsonify(resp)
    except Exception as e:
        logging.getLogger("api").error(
            "/cargoplanner/v1     fails %s", e)
        return jsonify({"err": traceback.format_exc()})

if __name__ == '__main__':
    CORS(app)
    app.debug = True
    app.run(host='0.0.0.0', port=5000, threaded=True)
