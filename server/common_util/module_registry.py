__author__ = 'crusaderwolf'


from flask import Flask
from common_util import util
"""
method to preload dependencies and initializing attributes.
Mongodb data connector initialized
"""


def boot_sequence():
    app = Flask(__name__)
    app.config['CORS_HEADERS'] = 'Content-Type'
    util.setup_logging()
    return app
