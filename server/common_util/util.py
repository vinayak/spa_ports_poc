__author__ = 'crusaderwolf'
import json
import logging
import logging.config
import os
import time
import datetime
import uuid

def jsonify_request(request):
    requestData = None
    try:
        requestData = json.loads(request.data.decode('utf-8'))
    except Exception as e:
        print(e)
        pass
    return requestData


def setup_logging(
        default_path='./config/logging.json',
        default_level=logging.INFO,
        env_key='LOG_CFG'):
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def get_timestamp():
    return datetime.datetime.utcnow()


def get_current_time():
    return time.time()

def get_rand_id():
    return str(uuid.uuid4())